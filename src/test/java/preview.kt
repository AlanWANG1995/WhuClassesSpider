import config.db_name
import config.db_pwd
import config.db_userId
import java.awt.Component
import javax.swing.JFrame
import javax.swing.JFrame.EXIT_ON_CLOSE

fun <T:Component> preview(ins: T): Unit {
    val jFrame = JFrame("preview")
    jFrame.add(ins)
    jFrame.defaultCloseOperation = EXIT_ON_CLOSE
    jFrame.pack()
    jFrame.isVisible = true
}

fun main(args: Array<String>) {
    preview(DataForm(DBconnector(db_userId, db_pwd, db_name)).rootPanel)
}