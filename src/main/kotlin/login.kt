import config.UserId
import config.outfileName
import org.jsoup.Connection
import org.jsoup.Jsoup
import java.io.*
import java.nio.charset.Charset
import java.nio.file.Files
import java.nio.file.Path
import javax.swing.ImageIcon
import kotlin.collections.ArrayList

const val BASE_URL = "http://210.42.121.132"
//const val PWD = "f4e6b9bfe71ea942930b1b13becad30e"
const val USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:29.0) Gecko/20100101 Firefox/29.0"

class Spider {
    private lateinit var cfg: UserId
    private lateinit var mCookies: MutableMap<String, String>
    internal fun captcha() {
        if (File("cache").exists()) {
            readCookie()
            if (checkCookieIllegal()) return
        }
        val ins = LoginDialog(null)
        ins.show()
        while (true) {
            //get captcha
            val con = Jsoup.connect("$BASE_URL/servlet/GenImg")
            con.header("User-Agent", USER_AGENT)
            val rs = con.ignoreContentType(true).execute()
            mCookies = rs.cookies()

            ins.refreshCap(ImageIcon(rs.bodyAsBytes()))
            cfg = ins.getmAuth()
            // try login
            val postCon = Jsoup.connect("$BASE_URL/servlet/Login")
            postCon.header("User-Agent", USER_AGENT)
            val rsLogin = postCon.ignoreContentType(true)
                    .method(Connection.Method.POST)
                    .data(hashMapOf("id" to cfg.id, "pwd" to cfg.pwd, "xdvfb" to cfg.cpt))
                    .cookies(mCookies).execute()
            if ({
                var res = true
                for (element in Jsoup.parse(rsLogin.body()).getElementsByTag("font")) {
                    if (element.text().contains("验证码错误")) {
                        res = false
                        ins.setMsg("验证码错误")
                    } else if (element.text().contains("用户名/密码错误")) {
                        res = false
                        ins.setMsg("用户名/密码错误")
                    }
                }
                res
            }.invoke()) {
                mCookies = rs.cookies()
                writeCookie()
                ins.requestExit()
                break
            }
        }
    }

    private fun writeCookie() {
        val cookcie = File("cache")
        val oos = ObjectOutputStream(FileOutputStream(cookcie))
        oos.writeObject(mCookies)
        oos.flush()
        oos.close()
    }

    private fun readCookie() {
        val cookcie = File("cache")
        val ois = ObjectInputStream(FileInputStream(cookcie))
        mCookies = ois.readObject() as MutableMap<String, String>
        ois.close()
    }

    private fun checkCookieIllegal() : Boolean{
        val crt_href = "choose_PubLsn_list.jsp"
        val con = Jsoup.connect("$BASE_URL/stu/$crt_href")
        val dom = con.header("User-Agent", USER_AGENT).cookies(mCookies).execute()
        for (element in Jsoup.parse(dom.body()).getElementsByTag("font")) {
            return if (element.text().contains("验证码错误")) {
                false
            } else if(element.text().contains("超时")) {
                false
            } else !element.text().contains("用户名/密码错误")
        }
        return false
    }

    internal fun updateLsn(): Boolean {
        var crt_href = "choose_PubLsn_list.jsp"
        val entryList = ArrayList<String>(400)
        do {
            val con = Jsoup.connect("$BASE_URL/stu/$crt_href")
            val dom = con.header("User-Agent", USER_AGENT).cookies(mCookies).execute().parse()

            val tables = dom.getElementsByTag("table").filter { it.attr("class") == "table listTable" }
            if (tables.isNotEmpty()) {
                val tbody = tables[0].getElementsByTag("tbody")[0]
                var entryCount = 1

                //add header
                if (entryList.isEmpty()) {
                    val headers = tbody.getElementsByTag("th").map { it.text() }
                    assert(headers.isNotEmpty())
                    entryList.add("ID\t" + headers.parJoin("\t", "'", "'"))
                }
                for (ele in tbody.getElementsByTag("tr")) {
                    entryCount++
                    var id = ""
                    val tdList = ele.getElementsByTag("td").map {
                        val inputTag = it.getElementsByTag("input")
                        if (inputTag.isNotEmpty()) {
                            id = inputTag.attr("id")
                            if (inputTag.hasAttr("enable")) "1" else "0"
                        } else {
                            it.text()
                        }
                    }
                    if (tdList.isEmpty()) continue
                    entryList.add("'$id'\t" + tdList.parJoin(separator = "\t", prefix = "'", postfix = "'",
                            chooser = { s -> !s.matches(Regex("\\d+")) }
                    ))
                }
            }

            val page_nav = dom.getElementsByTag("div").filter { it.attr("class") == "page_nav" }[0]
            val next = page_nav.getElementsByClass("navegate").filter {
                it.ownText().contains("下一页")
            }
            if (next.isEmpty()) {
                break
            } else {
                crt_href = next[0].attr("href")
            }
        } while (true)

        writeResult(outfileName, entryList)

        return true
    }

    private fun writeResult(filename: String, data: Iterable<String>): Unit {
        val file = File("data/$filename")
        if (!file.parentFile.exists()) {
            file.parentFile.mkdirs()
        }
        Files.write(file.toPath(), data, Charset.forName("UTF-8"))
    }
}

fun login(): Unit {
    val ins = Spider()
    ins.captcha()
    ins.updateLsn()
}
