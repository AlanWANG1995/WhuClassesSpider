import config.*
import java.awt.Font
import java.awt.Point
import java.io.BufferedReader
import java.io.FileReader
import javax.swing.JFrame
import javax.swing.JFrame.EXIT_ON_CLOSE
import javax.swing.UIManager
import javax.swing.plaf.FontUIResource

class App {
    private val mWindow = JFrame("lesson Chooser version $VERSION")
    private var tableStructure: SqlTableStructure = SqlTableStructure(
            name = TABLE_NAME,
            primary_key_name = "ID",
            fields = listOf("ID", "课程名", "学分", "剩余最大人数", "教师名", "职称", "授课学院", "教材", "学年", "学期", "上课时间地点", "备注", "选课"),
            types = listOf("BIGINT", "TEXT", "FLOAT", "TEXT", "TEXT", "TEXT", "TEXT", "TEXT", "INT", "TEXT", "TEXT", "TEXT", "BIT")
    )
    private lateinit var db: DBconnector

    init {
        setLAF()
        // fixme: cannot change font
        setDefaultFont()
        show()
    }

    private fun show() {
        login()
    }

    private fun loadFile() {
        val bs = BufferedReader(FileReader(dumpdataFilePath))
        var count = 0
        val table = db.findTableByName("lsns")
        for (line in bs.lines()) {
            if (count ++ == 0 ) continue
            table.insertOrUpdate(line.split("\t"))
        }
    }


    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            resolveConfig(args)
            val ins = App()
            ins.db = DBconnector(db_userId, db_pwd, db_name)
            ins.db.openOrCreateDatabase()
            ins.db.createTable(ins.tableStructure, !DEBUG)
            ins.loadFile()

            ins.mWindow.add(DataForm(ins.db).rootPanel)
            ins.mWindow.pack()
            ins.mWindow.defaultCloseOperation = EXIT_ON_CLOSE
            ins.mWindow.location = Point(200,200)
            ins.mWindow.isResizable = false
            ins.mWindow.isVisible = true
        }

        fun setLAF(lak: String = UIManager.getSystemLookAndFeelClassName()): Unit {
            UIManager.setLookAndFeel(lak)
        }

        fun setDefaultFont(font: FontUIResource = FontUIResource(Font("Microsoft YaHei", Font.PLAIN, 18))) {
            val keys = UIManager.getDefaults().keys()
            while (keys.hasMoreElements()) {
                val key = keys.nextElement()
                val value = UIManager.get(key)
                if (value is FontUIResource)
                    UIManager.put(key, font)
            }
        }
    }
}