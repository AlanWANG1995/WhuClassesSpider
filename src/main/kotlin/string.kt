import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

fun Iterable<String>.parJoin(separator: String = ",", prefix: String = "", postfix: String = "", chooser:(String)->Boolean = {true}): String{

    val reFix = this.map {
        if (chooser(it))
        "$prefix$it$postfix"
        else it
    }
    return reFix.joinToString(separator = separator)
}

//f4e6b9bfe71ea942930b1b13becad30e
fun md5(b: Any): String {
    val hexDigits = charArrayOf('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f')
    fun bytesToHex(bytes: ByteArray): String {
        val sb = StringBuffer()
        var t: Int
        for (i in 0..15) {
            t = bytes[i].toInt()
            if (t < 0)
                t += 256
            sb.append(hexDigits[t.ushr(4)])
            sb.append(hexDigits[t % 16])
        }
        return sb.toString()
    }
    val md = MessageDigest.getInstance(System.getProperty(
            "MD5.algorithm", "MD5"))
    return bytesToHex(md.digest("$b".toByteArray(charset("utf-8"))))
}