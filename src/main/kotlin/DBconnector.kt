import com.mysql.cj.api.MysqlConnection
import com.mysql.cj.jdbc.MysqlDataSource
import com.mysql.cj.x.core.MysqlxSession
import config.DEBUG
import config.TABLE_NAME
import db.models.Table
import exceptions.TypeMissMatchException
import java.sql.Connection
import kotlin.Deprecated

class DBconnector(var userID: String, var pwd: String, var database: String) {
    private val source = MysqlDataSource()
    private val tableNames = ArrayList<String>()
    private val tableStructures = HashMap<String, SqlTableStructure>(3)

    init {
        source.user = userID
        source.setPassword(pwd)
        openOrCreateDatabase()
    }

    fun findTableByName(name: String): Table {
        var priKey: String = ""
        val colNames = ArrayList<String>(10)
        val colTypes = ArrayList<String>(10)
        connect {
            val sets = it.metaData.getColumns(database, database, name, "%")
            val primaryKey = it.metaData.getPrimaryKeys(database, database, name)
            val hasNext = primaryKey.next()
            assert(hasNext, { "No primary key provided!" })
            priKey = primaryKey.getString(4)
            while (sets.next()) {
                colNames.add(sets.getString(4)) //table name
                colTypes.add(sets.getString(6)) //column type
            }
        }

        return Table(
                SqlTableStructure(
                        name = name,
                        primary_key_name = priKey,
                        fields = colNames,
                        types = colTypes
                ),
                this
        )
    }

    internal fun openOrCreateDatabase(): Boolean {
        var res = true
        source.connection.use {
            val sql = """
                CREATE DATABASE IF NOT EXISTS $database;
            """
            res = res and it.createStatement().execute(sql)
        }
        source.databaseName = database
        return res
    }

    internal fun cleanDatabase() {
        val s = "DROP DATABASE IF EXISTS $database;"
        source.connection.use {
            it.createStatement().execute(s)
        }
    }

    internal inline fun <T> connect(block: (Connection) -> T): T {
        val a = source.connection.use {
            block.invoke(it)
        }
        return a
    }

    fun getConnection(): Connection {
        return source.connection
    }

    @Deprecated("instead of using [Table] model")
    fun insertOrUpdate(values: Collection<String>, table: SqlTableStructure): Int {
        val s = """
            INSERT INTO $TABLE_NAME
              ${table.header()}
            VALUES
              ${values.joinToString(prefix = "(", postfix = ")")}
            ON DUPLICATE KEY UPDATE
              ${table.headers().zip(values.drop(1)).map { "${it.first} = ${it.second}" }.joinToString()}
        """
        var i = 0
        source.connection.use {
            try {
                i = it.prepareStatement(s).executeUpdate()
            } catch (e: Exception) {
                println(s)
                println(e)
            }
        }
        return i
    }

    internal fun createTable(table: SqlTableStructure, forceRecreate: Boolean = false) {
        source.connection.use {
            if (forceRecreate and (this.tableNames.contains(table.name))) {
                val del_sql = "DROP TABLE ${table.name} ;"
                it.prepareStatement(del_sql).execute()
            }
            val sql = "CREATE TABLE IF NOT EXISTS $TABLE_NAME $table DEFAULT CHARSET 'utf8';"
            it.prepareStatement(sql).execute()
        }
    }
}

data class SqlTableStructure(
        val name: String,
        var primary_key_name: String,
        var fields: Collection<String>,
        var types: Collection<String>
) {

    init {
        if (fields.size != types.size) {
            throw TypeMissMatchException("Length of fileds(size: ${fields.size})" +
                    " is not equal to types(size: ${types.size})")
        }
    }

    override fun toString(): String {
        val mTable = ArrayList<String>(12)
        fields.zip(types).mapTo(mTable) {
            if (it.first == primary_key_name) {
                "${it.first} ${it.second} PRIMARY KEY"
            } else {
                "${it.first} ${it.second}"
            }
        }
        return mTable.joinToString(separator = ",", prefix = "(", postfix = ")")
    }

    fun header(): String {
        return "(${fields.joinToString()})"
    }

    fun headers(): Collection<String> {
        return fields
    }

    fun fieldType(field: String): String {
        return types.elementAt(field.indexOf(field))
    }

    fun columnsCount(): Int {
        return fields.size
    }
}