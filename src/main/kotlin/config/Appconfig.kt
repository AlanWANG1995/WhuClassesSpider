package config

const val VERSION = "1.0.0-alpha"
val dumpdataFilePath = "data/$outfileName"
var DEBUG = false

fun resolveConfig(args: Array<String>): Unit {
    for (i in args){
        when(i){
            "-debug" ->{
                DEBUG = true
            }
        }
    }
}