package config

import md5

data class UserId(val id:String, var pwd: String, var cpt: String){
    init {
        pwd = md5(pwd)
    }
}