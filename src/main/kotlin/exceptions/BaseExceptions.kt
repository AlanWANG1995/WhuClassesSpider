package exceptions

class TypeMissMatchException(val msg: String):Exception(msg)