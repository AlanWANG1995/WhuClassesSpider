package db.models

typealias TEXT=String
typealias INT=Int
typealias BIGINT=Long
typealias BIT=Boolean