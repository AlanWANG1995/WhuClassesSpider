package db.models

import SqlTableStructure
import DBconnector
import com.mysql.cj.api.mysqla.result.Resultset
import config.TABLE_NAME
import exceptions.TypeMissMatchException
import java.sql.ResultSet

class Table(val structure: SqlTableStructure, val owner: DBconnector) {

    fun insertOrUpdate(data: List<Any>) = insertOrUpdate(Row(data, structure))
    fun insertOrUpdate(data: Row){
        owner.connect {
            val s = """
            INSERT INTO $TABLE_NAME
              ${data.header}
            VALUES
              ${data.value}
            ON DUPLICATE KEY UPDATE
              ${data.dict}
            """
            it.prepareStatement(s).execute()
        }
    }

    //select
    fun select(colName: String, selector : String = "") : ResultSet{
        val mResult : ResultSet = owner.getConnection().let {
            val s = "SELECT $colName FROM ${structure.name} $selector;"
            it.prepareStatement(s).executeQuery()
        }

        return mResult
    }

    /**
     * Returns a string representation of the object.
     */
    override fun toString(): String {
        return structure.toString()
    }

    class Row(private var data: List<Any>, private val structure: SqlTableStructure){
        var header = structure.header()
        var value = data.joinToString(prefix = "(", postfix = ")")
        var dict = structure.headers().zip(data).map { "${it.first} = ${it.second}" }.joinToString()
        init {
            if (!data.matchType(structure)) {
                throw TypeMissMatchException("Type mismatched")
            }
        }
    }
}

fun List<Any>.matchType(structure: SqlTableStructure): Boolean{
    // check length
    if (this.size != structure.columnsCount()){
        throw TypeMissMatchException("Length of values(size: ${this.size})" +
                " mismatches columns(size: ${structure.columnsCount()})")
    }
    
    fun checkForType(v: Any, e:String) : Boolean {
        when (v) {
            is String, is CharSequence, is CharArray -> {
                if (v.toString().matches(Regex("\\d+"))){
                    val length = v.toString().length
                    when {
                        length == 1 -> return checkForType(v==1, e)
                        length < 10 -> return checkForType(v.toString().toInt(), e)
                        length < 23 -> return checkForType(v.toString().toLong(), e)
                    }
                }
                return ("TEXT" == e)
            }
            is Long -> {
                return ("BIGINT" == e)
            }
            is Int -> {
                return ("INT" == e)
            }
            is Boolean -> {
                return ("BIT" == e)
            }
            else -> {
                return false
            }
        }
    }

    val types = structure.types
    var res = true
    for (value in this.zip(types)) {
        res = checkForType(value.first, value.second)
    }
    return res
}

fun List<Any>.toSql(): ArrayList<String> {
    val res = ArrayList<String>(12)
    this.mapTo(res){
        when(it){
            is Long, is Int, is Float, is Double -> {
                it.toString()
            }
            is Boolean -> {
                if(it){
                    "1"
                } else {
                    "0"
                }
            }
            is String -> {
                "'$it'"
            }
            else -> {
                throw java.sql.SQLDataException("Unsupported type ${it::class.qualifiedName}")
            }
        }
    }
    return res
}