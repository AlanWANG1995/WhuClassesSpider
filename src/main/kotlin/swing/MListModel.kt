package swing

import javax.swing.DefaultListModel


class  MListModel<T>: DefaultListModel<T>() {
}


data class TableRow(
        var owner : Long,
        var id : Long,
        var name : String,
        var prof : String
){
    /**
     * Returns a string representation of the object.
     */
    override fun toString(): String {
        return "$name  |  $prof"
    }
}