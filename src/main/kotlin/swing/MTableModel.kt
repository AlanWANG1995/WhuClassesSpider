package swing

import javax.swing.table.DefaultTableModel
import DBconnector
import db.models.Table
import java.sql.ResultSet
import java.util.*
import javax.management.Query

class MTableModel(private var db: DBconnector? = null) : DefaultTableModel() {
    private var tableName = "lsns"

    init {
        this.addTableModelListener({

        })
        val table = db?.findTableByName(tableName)
        if (table != null){
            table.structure
            setData(table.select("*"))
        }
    }



    override fun isCellEditable(row: Int, column: Int): Boolean {
        return false
    }

    fun setData(results: ResultSet) {
        val colCount = results.metaData.columnCount
        val cVec = Vector<String>(colCount)
        (1..colCount).mapTo(cVec) { results.metaData.getColumnLabel(it) }

        val rVec = Vector<Vector<String>>(100)
        while (results.next()){
            val crtVec = Vector<String>(colCount)
            (1..colCount).mapTo(crtVec) { results.getString(it) }
            rVec.addElement(crtVec)
        }
        this.setDataVector(rVec, cVec)
    }

    fun query(query: String){
        val table = db?.findTableByName(tableName)!!
        var sql = query
        // colname is no used
        try {
            for (matchResult in Regex("""\$(\d{1,2})""").findAll(sql).iterator()) {
                sql = sql.replace(matchResult.groupValues[0], columnIdentifiers[matchResult.groupValues[1].toInt() - 1].toString())
            }
            setData(table.select(colName = "*", selector = sql))
        } catch (e:Exception){
            e.printStackTrace()
        }
    }
}