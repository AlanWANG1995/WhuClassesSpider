package swing

import com.sun.javafx.iio.common.ImageTools

import javax.imageio.ImageIO
import javax.swing.*
import java.awt.*

class MPanel : JPanel() {

    override fun paintComponent(g: Graphics) {
        val image = ImageIO.read(ClassLoader.getSystemClassLoader().getResource("bgs/7718705a451827affeecef738f5089320c467b0c.jpg"))
        val scaledImage = image.getScaledInstance(674,480, Image.SCALE_SMOOTH)
        g.drawImage(scaledImage,0,0,this)
    }
}

fun main(args: Array<String>) {
    JFrame().apply{
        add(MPanel())
        pack()
        isVisible = true
    }
}


