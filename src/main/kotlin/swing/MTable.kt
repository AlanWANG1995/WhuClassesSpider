package swing

import javax.swing.JTable
import javax.swing.SwingUtilities

internal open class MTable : JTable() {
    init {
        this.setAutoCreateColumnsFromModel(true)
        SwingUtilities.invokeLater {
            this.setRowHeight((this.getFontMetrics(this.font).height * 1.5).toInt())
        }
    }

    fun mModel(): MTableModel {
        return this.model as MTableModel
    }
}