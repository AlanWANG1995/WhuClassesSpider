import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

group = "AlanWORK"
version = "1.0-SNAPSHOT"

buildscript {
    var kotlin_version: String by extra
    kotlin_version = "1.2.0"

    repositories {
        mavenCentral()
    }

    dependencies {
        classpath(kotlinModule("gradle-plugin", kotlin_version))
    }

}

apply {
    plugin("kotlin")
}

val kotlin_version: String by extra

repositories {
    mavenCentral()
}

dependencies {
    compile(kotlinModule("stdlib-jdk8", kotlin_version))
    compile("mysql", "mysql-connector-java", "6.0.6")
    compile("org.jsoup", "jsoup", "1.9.2")
    compile("org.jetbrains.kotlin","kotlin-reflect", "1.2.0")
    compile(files("lib/guidesigner.jar"))
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

